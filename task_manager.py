import datetime as dt
from pydantic import BaseModel
from trajectory_controller import TrajectoryController
import ephem
import time


class TLE(BaseModel):
    line0: str
    line1: str
    line2: str

    @property
    def ephem_object(self):
        return ephem.readtle(self.line0, self.line1, self.line2)


class OrientationCalculator:
    def __init__(self, gs_orientation: tuple[float, float], tle: TLE):
        self.gs_lattitude, self.gs_longitude = gs_orientation
        self.tle: TLE = tle

    def get_orientation(self, timestamp):
        self.tle.ephem_object.compute(timestamp)
        azimuth, elevation = 0, 0
        return azimuth, elevation


def follow(
        calculator: OrientationCalculator, 
        stop: dt.datetime
    ):
    tc = TrajectoryController()
    timestamp = dt.datetime.now()
    while timestamp <= stop:
        timestamp = dt.datetime.now()
        azimuth, elevation = calculator.get_orientation(timestamp)
        print(f"{timestamp} | a: {azimuth}, e: {elevation}")
        tc.set_orientation(azimuth, elevation)
        time.sleep(3)


tle = TLE(
    line0 = "2023-091P",
    line1 = "1 57179U 23091P   24168.93728969  .00005077  00000-0  34590-3 0  9991",
    line2 = "2 57179  97.6168 220.8745 0020029 115.9899 244.3394 15.06550557 53434"
)
follow(
    OrientationCalculator((51.107, 17.062), tle),
    dt.datetime.now() + dt.timedelta(seconds=60)
)
