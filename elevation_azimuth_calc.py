import math
import numpy as np
from sgp4.api import Satrec, jday
import datetime
""""
Steps to convert satellite's TLE data and station's GCS to the antenna's elevation and azimuth
1. Convert satellite's TLE to the ECEF format
2. Canvert station's GCS to ECEF format
3. Calculate the station-satelite vector (satelltite ECEF - station ECEF)
4. Convert the stat-sat vector to ENU format
5. Calculate elevation where 0 deg is horizon line
6 Calculate azimuth (where 0 deg is N pole )
"""

def geodetic_to_ecef(lat, lon, alt):
    """Convert geodetic coordinates to ECEF coordinates."""
    # WGS84 ellipsoid constants
    a = 6378137.0           # semi-major axis in meters
    f = 1 / 298.257223563   # flattening
    b = a * (1 - f)         # semi-minor axis
    e2 = 1 - (b**2 / a**2)  # first eccentricity squared

    # Convert latitude, longitude from degrees to radians
    lat_rad = math.radians(lat)
    lon_rad = math.radians(lon)

    # Compute prime vertical radius of curvature
    N = a / math.sqrt(1 - e2 * (math.sin(lat_rad)**2))

    # Compute ECEF coordinates
    X = (N + alt) * math.cos(lat_rad) * math.cos(lon_rad)
    Y = (N + alt) * math.cos(lat_rad) * math.sin(lon_rad)
    Z = ((1 - e2) * N + alt) * math.sin(lat_rad)

    return np.array([X, Y, Z])

"""Convert ECEF coordinates to ENU coordinates."""
def ecef_to_enu(ecef_sat, ecef_obs, lat, lon):
    
    # Convert latitude and longitude from degrees to radians
    lat_rad = math.radians(lat)
    lon_rad = math.radians(lon)

    # Calculate the difference
    diff = ecef_sat - ecef_obs

    # Define the transformation matrix from ECEF to ENU
    trans_matrix = np.array([
        [-math.sin(lon_rad),              math.cos(lon_rad),             0],
        [-math.sin(lat_rad) * math.cos(lon_rad), -math.sin(lat_rad) * math.sin(lon_rad), math.cos(lat_rad)],
        [ math.cos(lat_rad) * math.cos(lon_rad),  math.cos(lat_rad) * math.sin(lon_rad), math.sin(lat_rad)]
    ])

    # Transform the difference vector to ENU coordinates
    enu = np.dot(trans_matrix, diff)

    return enu

def calculate_elevation_azimuth(enu_coords):
    """Calculate elevation and azimuth from ENU coordinates."""
    east, north, up = enu_coords
    horizontal_dist = math.sqrt(east**2 + north**2)
    elevation = math.degrees(math.atan2(up, horizontal_dist))
    azimuth = math.degrees(math.atan2(east, north))
    if azimuth < 0:
        azimuth += 360
    return elevation, azimuth

# TLE data for the satellite
line1 = "1 57179U 23091P   24168.93728969  .00005077  00000-0  34590-3 0  9991"
line2 = "2 57179  97.6168 220.8745 0020029 115.9899 244.3394 15.06550557 53434"

# Create satellite object
satellite = Satrec.twoline2rv(line1, line2)

# Select a time for the computation (current time in this example)
now = datetime.datetime.now()
jd, fr = jday(now.year, now.month, now.day, now.hour, now.minute, now.second + now.microsecond * 1e-6)

# Propagate the TLE to get position and velocity in TEME frame
e, r_teme, v_teme = satellite.sgp4(jd, fr)

if e != 0:
    raise RuntimeError(f"SGP4 propagation error: {e}")

def gstime(jd_ut1):
    """Calculate Greenwich Sidereal Time from Julian Date (UT1)."""
    T = (jd_ut1 - 2451545.0) / 36525.0
    gmst = 280.46061837 + 360.98564736629 * (jd_ut1 - 2451545.0) + 0.000387933 * T**2 - T**3 / 38710000.0
    gmst = gmst % 360.0
    if gmst < 0:
        gmst += 360.0
    return np.deg2rad(gmst)

def teme_to_ecef(r_teme, jd_ut1):
    gst = gstime(jd_ut1)
    
    c = np.cos(gst)
    s = np.sin(gst)
    
    R = np.array([
        [ c, s, 0],
        [-s, c, 0],
        [ 0, 0, 1]
    ])
    
    r_ecef = np.dot(R, r_teme)
    
    return r_ecef

# Convert TEME coordinates to ECEF
r_ecef = teme_to_ecef(r_teme, jd + fr)

# Example observer location (latitude, longitude, altitude in meters)
obs_lat = 37.7749  # degrees
obs_lon = -122.4194  # degrees
obs_alt = 10.0  # meters

# Compute observer's ECEF coordinates
obs_ecef = geodetic_to_ecef(obs_lat, obs_lon, obs_alt)

# Compute ENU coordinates
enu_coords = ecef_to_enu(r_ecef, obs_ecef, obs_lat, obs_lon)

# Calculate elevation and azimuth angles
elevation_angle, azimuth_angle = calculate_elevation_azimuth(enu_coords)

print(f"Elevation Angle: {elevation_angle:.2f} degrees")
print(f"Azimuth Angle: {azimuth_angle:.2f} degrees")
