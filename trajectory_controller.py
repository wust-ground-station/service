import paho.mqtt.publish as publish
import time    


class TrajectoryController:
    def __init__(self):
        self.topic = "rotor/orientation"
        self.server = "kalenica.local"

    def set_home(self):
        payload = f"home"
        publish.single(self.topic, payload, hostname=self.server)

    def set_orientation(self, azimuth: float, elevation: float):
        if 0 > azimuth <= 360:
            raise Exception(f"azimuth: {azimuth}")
        if 0 > elevation <= 180:
            raise Exception(f"elevation: {elevation}")

        payload = f"a: {azimuth}, e: {elevation}"
        publish.single(self.topic, payload, hostname=self.server)


if __name__ == "__main__":
    tc = TrajectoryController()
    while True:
        epoch_time = int(time.time())
        azimuth = epoch_time % 360
        elevation = epoch_time % 180
        tc.set_orientation(azimuth, elevation)
        time.sleep(1)
